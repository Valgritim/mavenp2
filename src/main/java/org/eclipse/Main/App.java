package org.eclipse.Main;

import java.util.List;

import org.eclipse.model.Personne;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
//    	Personne personne = new Personne();
//    	personne.setNom("Potter");
//    	personne.setPrenom("Harry");
    	Configuration configuration = new Configuration().configure();
    	//On n'est pas obligé de mettre le mapping dans hibernate.cfg.xml
    	//on peut ecrire comme ça: configuration.addAnnotatedClass(Personne.class);
    	//SEssionFactory(Hibernate) <=> l'Entity manager(JPA)
    	SessionFactory sessionFactory = configuration.buildSessionFactory();
    	Session session = sessionFactory.openSession();
    	Transaction transaction = session.beginTransaction();
    	
//    	Integer cle = (Integer) session.save(personne);
//    	session.persist(personne);
//    	transaction.commit();
//    	session.close();
//    	sessionFactory.close();
//    	System.out.println(cle);
    	
    	//Pour chercher une personne: load ou get => get retourne null si pas trouvé et load déclenche une exception
    	
//        Personne personne1 = session.load(Personne.class, 1);
//        System.out.println(personne1);
//        transaction.commit();
//    	session.close();
//    	sessionFactory.close();
    	
    	//Modifier une personne
    	
//    	Personne personne3 = session.get( Personne.class,2);
//    	personne3.setNom("Williams");
    	//Flush envoie toutes les modifs des entités managées dans la BDD
    	//A la difference de save(obj) ou persist(obj) ui  prennent en param l'objet à modifier
//    	session.flush();
//    	transaction.commit();
//    	session.close();
//    	sessionFactory.close();
    	
    	//Supprimer un tupple
//    	Personne personne1 =  session.get(Personne.class, 1);
//    	session.delete(personne1);
//    	transaction.commit();
//    	session.close();
//    	sessionFactory.close();
    	
    	//Récupérer la liste de toutes les personnes
//    	String string = "Harry";
//    	Criteria criteria = session.createCriteria(Personne.class);    	
//    	criteria = criteria.add(Restrictions.eq("prenom", string));
//    	List<Personne> personnes = criteria.list();
//    	for(Personne personne:personnes)
//    		System.out.println(personne);
    	
//    	transaction.commit();
    	
//    	Personne p = session.load(Personne.class, 2);
//    	p.setNom("Travolta");
//    	session.refresh(p);
//    	System.out.println("le nom est " + p.getNom());
//    	// affiche le nom est Wick
//    	transaction.commit();
//    	session.close();
//    	sessionFactory.close();
    	
//    	String hql = "SELECT * from Personne WHERE nom= :nom";
//    	
//    	SQLQuery query = session.createSQLQuery(hql);
//    	query.addEntity(Personne.class);
//    	query.setParameter("nom", "Potter");
//    	
//    	List<Personne> personnes = query.list();
//    	for(Personne personne : personnes)
//    		System.out.println(personne);
//    	
//    	transaction.commit();
//    	session.close();
//    	sessionFactory.close();
    	
    	Query query = session.getNamedQuery("findByNomPrenom");
    	query.setParameter("nom", "Potter");
    	query.setParameter("prenom", "Harry");
    	
    	List<Personne> personnes = query.list();
    	for(Personne personne:personnes)
    		System.out.println(personne);
    	
    	Query query2 = session.getNamedQuery("findByPrenom");
    	query2.setParameter("prenom", "Lucius");
    	
    	List<Personne> personnes2 = query2.list();
    	for(Personne person:personnes2)
    		System.out.println(person);
    	
    	transaction.commit();
    	session.close();
    	sessionFactory.close();
    	
    }
    
}

