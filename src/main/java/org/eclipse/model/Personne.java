package org.eclipse.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@NamedQueries({
@NamedQuery(
		name="findByNomPrenom",
		query="SELECT p FROM Personne p WHERE p.nom = :nom and p.prenom = :prenom"
		),
@NamedQuery(
		name="findByPrenom",
		query="SELECT p FROM Personne p WHERE p.prenom = :prenom"
		),})

public class Personne {
	
	@Id
	@GeneratedValue (strategy=GenerationType.IDENTITY)
	private int num;
	private String nom;
	private String prenom;
	
	public Personne(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}

	public Personne() {
		super();
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Personne [num=" + num + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
	
	
}
